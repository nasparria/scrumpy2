# Generated by Django 4.0.5 on 2022-06-03 19:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('recipes', '0010_ingredient_amount'),
    ]

    operations = [
        migrations.AlterField(
            model_name='step',
            name='food_items',
            field=models.ManyToManyField(blank=True, to='recipes.fooditem'),
        ),
    ]
