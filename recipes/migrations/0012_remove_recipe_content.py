# Generated by Django 4.0.5 on 2022-06-03 20:05

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('recipes', '0011_alter_step_food_items'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='recipe',
            name='content',
        ),
    ]
